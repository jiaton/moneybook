package com.jt.moneybook.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatConverter {
    public static String convert(Date date) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat converter = new SimpleDateFormat(pattern);
        return converter.format(date);
    }
}
