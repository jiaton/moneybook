package com.jt.moneybook;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.jt.moneybook.Fragments.CostFragment;

import java.util.UUID;

public class CostActivity extends SingleFragmentActivity {

    private static final String EXTRA_COST_ID = "com.jt.moneybook.cost_id";


    @Override
    protected Fragment createFragment() {
        UUID costId = (UUID) getIntent().getSerializableExtra(EXTRA_COST_ID);
        return CostFragment.newInstance(costId);

    }

    public static Intent newIntent(Context packageContext, UUID costId) {
        Intent intent = new Intent(packageContext, CostActivity.class);
        intent.putExtra(EXTRA_COST_ID, costId);
        return intent;
    }
}
