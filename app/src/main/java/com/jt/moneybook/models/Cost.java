package com.jt.moneybook.models;

import java.util.Date;
import java.util.UUID;

public class Cost {
    private UUID mId;
    private String mTitle;
    private Date mDate;
    private String mMoney;
    private String mDirection;
    private String mMore;
    private String mMethod;

    public Cost(UUID id) {
        mId = id;
        mDate = new Date();
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getMoney() {
        return mMoney;
    }

    public void setMoney(String money) {
        this.mMoney = money;
    }

    public String getDirection() {
        return mDirection;
    }

    public void setDirection(String direction) {
        mDirection = direction;
    }

    public String getMore() {
        return mMore;
    }

    public void setMore(String more) {
        mMore = more;
    }

    public String getMethod() {
        return mMethod;
    }

    public void setMethod(String method) {
        mMethod = method;
    }
}
