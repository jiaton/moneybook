package com.jt.moneybook.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.jt.moneybook.database.CostBaseHelper;
import com.jt.moneybook.database.CostCursorWrapper;
import com.jt.moneybook.database.CostDbSchema.CostTable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

public class CostLab {
    public static final int CONDITION_BY_ALL = 0;
    public static final int CONDITION_BY_YEAR = 1;
    public static final int CONDITION_BY_MONTH = 2;
    public static final int CONDITION_BY_DAY = 3;

    private static CostLab sCostLab;

    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static CostLab get(Context context) {
        if (sCostLab == null) {
            sCostLab = new CostLab(context);
        }
        return sCostLab;
    }

    private CostLab(Context context) {
        /*构造函数为什么要传入context？*/
        mContext = context.getApplicationContext();
        mDatabase = new CostBaseHelper(mContext).getWritableDatabase();


    }

    public void addCost(Cost c) {
        ContentValues values = getContentValues(c);

        mDatabase.insert(CostTable.NAME, null, values);
    }

    public void deleteCost(Cost c) {
        mDatabase.delete(CostTable.NAME, CostTable.Cols.UUID + "=?", new String[]{c.getId().toString()});
    }

    public List<Cost> getCosts() {
        List<Cost> costs = new ArrayList<>();

        CostCursorWrapper cursor = queryCosts(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                costs.add(cursor.getCost());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return costs;

    }

    //2018/12/23 getCosts(Date, String) 根据年、月、日查询
    public List<Cost> getCosts(Date date, int conditionCode) {
        List<Cost> costs = new ArrayList<>();

        //先把date解析成年月日
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        Date date1 = null;
        Date date2 = null;
        switch (conditionCode) {
            case CONDITION_BY_YEAR:
                date1 = new GregorianCalendar(year, 0, 1).getTime();
                date2 = new GregorianCalendar(year + 1, 0, 1).getTime();
                break;
            case CONDITION_BY_MONTH:
                date1 = new GregorianCalendar(year, month, 1).getTime();
                date2 = new GregorianCalendar(year, month + 1, 1).getTime();
                break;
            case CONDITION_BY_DAY:
                date1 = new GregorianCalendar(year, month, day).getTime();
                date2 = new GregorianCalendar(year, month, day + 1).getTime();
                break;
            default:
                date1 = new Date(0);
                date2 = new Date(1000, 0, 1);
        }

        //查询数据库代码
//        CostCursorWrapper cursor  = new CostCursorWrapper(mDatabase.rawQuery("select * from " + CostTable.NAME + " where " + CostTable.Cols.DATE + " =? ",new String[]{"1545463539461"}));

        CostCursorWrapper cursor = queryCosts(CostTable.Cols.DATE + " >= ? and " + CostTable.Cols.DATE + " <? ", new String[]{Long.toString(date1.getTime()), Long.toString(date2.getTime())});
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                costs.add(cursor.getCost());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return costs;
    }
    public Cost getCost(UUID uuid) {
        CostCursorWrapper cursor = queryCosts(
                CostTable.Cols.UUID + "=?",
                new String[]{uuid.toString()}
        );
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getCost();
        } finally {
            cursor.close();
        }
    }

    public void updateCost(Cost cost) {
        String uuidString = cost.getId().toString();
        ContentValues values = getContentValues(cost);
        mDatabase.update(CostTable.NAME, values, CostTable.Cols.UUID + "=?",
                new String[]{uuidString});
    }

    private CostCursorWrapper queryCosts(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                CostTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new CostCursorWrapper(cursor);
    }

    private static ContentValues getContentValues(Cost cost) {
        /*将一个cost类转换成ContentValue，即可插入数据库*/
        ContentValues values = new ContentValues();
        values.put(CostTable.Cols.UUID, cost.getId().toString());
        values.put(CostTable.Cols.DATE, cost.getDate().getTime());
        values.put(CostTable.Cols.TITLE, cost.getTitle());
        values.put(CostTable.Cols.MONEY, cost.getMoney());
        values.put(CostTable.Cols.METHOD, cost.getMethod());
        values.put(CostTable.Cols.DIRECTION, cost.getDirection());
        values.put(CostTable.Cols.MORE, cost.getMore());

        return values;
    }

}
