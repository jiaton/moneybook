package com.jt.moneybook;

import android.support.v4.app.Fragment;

import com.jt.moneybook.Fragments.CostListFragment;

public class CostListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new CostListFragment();
    }
}
