package com.jt.moneybook.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.jt.moneybook.models.Cost;
import com.jt.moneybook.models.CostLab;
import com.jt.moneybook.R;
import com.jt.moneybook.utils.DateFormatConverter;

import java.util.Date;
import java.util.UUID;




public class CostFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_COST_ID = "cost_id";
    private static final String ARG_ACTION = "action";
    private static final String DIALOG_DATE = "DialogDate";

    private static final int REQUEST_DATE = 0;

    private Cost mCost;
    private FloatingActionButton mFab;
    private EditText mTitleField;
    private EditText mMoneyField;
    private Button mDateButton;
    private RadioGroup mMethodRadioGroup;
    private RadioGroup mDirectionRadioGroup;
    private TextView mMoreField;
    private Button mConfirmButton;
    private Button mDeleteButton;

    public static CostFragment newInstance(UUID costId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_COST_ID,costId);
        CostFragment fragment = new CostFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            CostLab.get(getActivity()).deleteCost(mCost);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID costId = (UUID) getArguments().getSerializable(ARG_COST_ID);
        mCost = CostLab.get(getActivity()).getCost(costId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cost, container, false);
        mFab = (FloatingActionButton) getActivity().findViewById(R.id.fab_add);
        mFab.hide();
        mTitleField = (EditText)v.findViewById(R.id.cost_title);
        mTitleField.setText(mCost.getTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mCost.setTitle(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mMoneyField = (EditText) v.findViewById(R.id.cost_money);
        mMoneyField.setText(mCost.getMoney());
        mMoneyField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mCost.setMoney(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mDateButton = (Button)v.findViewById(R.id.cost_date);
        updateDate();
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(mCost.getDate());
                dialog.setTargetFragment(CostFragment.this, REQUEST_DATE);
                dialog.show(manager, DIALOG_DATE);
            }
        });
        mMethodRadioGroup = v.findViewById(R.id.method_radiogroup);
        if ("信用卡".equals(mCost.getMethod())) {
            mMethodRadioGroup.check(R.id.credit_radio);
        }
        if ("现金".equals(mCost.getMethod())) {
            mMethodRadioGroup.check(R.id.cash_radio);
        }
        mMethodRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                View radioButton = radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
                int radioId = radioGroup.indexOfChild(radioButton);
                RadioButton btn = (RadioButton) radioGroup.getChildAt(radioId);
                String methodText = (String) btn.getText();
                mCost.setMethod(methodText);
            }
        });

        mDirectionRadioGroup = v.findViewById(R.id.in_out_group);
        if ("收入".equals(mCost.getDirection())) {
            mDirectionRadioGroup.check(R.id.in_radio);
        }
        if ("支出".equals(mCost.getDirection())) {
            mDirectionRadioGroup.check(R.id.out_radio);
        }
        mDirectionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                View radioButton = radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
                int radioId = radioGroup.indexOfChild(radioButton);
                RadioButton btn = (RadioButton) radioGroup.getChildAt(radioId);
                String directionText = (String) btn.getText();
                mCost.setDirection(directionText);
            }
        });
        mMoreField = v.findViewById(R.id.more_text);
        mMoreField.setText(mCost.getMore());
        mMoreField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mCost.setMore(charSequence.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mConfirmButton = v.findViewById(R.id.confirm_button);
        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CostLab.get(getActivity()).updateCost(mCost);
                getActivity().onBackPressed();
            }
        });
        mDeleteButton=v.findViewById(R.id.delete_button);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CostLab.get(getActivity()).deleteCost(mCost);
                getActivity().onBackPressed();
            }
        });
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_DATE){
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            mCost.setDate(date);
            updateDate();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

//        CostLab.get(getActivity()).updateCost(mCost);
    }

    private void updateDate() {
        mDateButton.setText(DateFormatConverter.convert(mCost.getDate()));
    }
}
