package com.jt.moneybook.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.jt.moneybook.models.CostLab;
import com.jt.moneybook.R;

import java.util.Date;


public class SearchFragment extends DialogFragment {
    private static final int REQUEST_DATE = 1;
    public static final String EXTRA_SEARCH_DATE = "com.jt.moneybook.searchDate";
    public static final String EXTRA_SEARCH_CONDITION = "com.jt.moneybook.searchCondition";

    private static final String DIALOG_DATE = "DialogDate";

    private Button mSelectDateButton;
    private RadioGroup mConditionRadioGroup;

    private Date mSearchDate = new Date();
    private int mConditionCode;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        /*
         * 应该有一些变量存储查询方式
         * 和选择的时间
         * 放到extra里
         * 跳转到新fragment显示搜索结果*/
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_search, null);
        mSelectDateButton = (Button) v.findViewById(R.id.select_date_button);
        mSelectDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(new Date());
                dialog.setTargetFragment(SearchFragment.this, REQUEST_DATE);
                dialog.show(manager, DIALOG_DATE);
            }
        });

        mConditionRadioGroup = (RadioGroup) v.findViewById(R.id.condition_radiogroup);
        mConditionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int radioId = radioGroup.getCheckedRadioButtonId();
                switch (radioId) {
                    case R.id.by_year_radiobutton:
                        mConditionCode = CostLab.CONDITION_BY_YEAR;
                        break;
                    case R.id.by_month_radiobutton:
                        mConditionCode = CostLab.CONDITION_BY_MONTH;
                        break;
                    case R.id.by_day_radiobutton:
                        mConditionCode = CostLab.CONDITION_BY_DAY;
                        break;
                    case R.id.by_all_radiobutton:
                        mConditionCode = CostLab.CONDITION_BY_ALL;
                        break;
                    default:
                        mConditionCode = CostLab.CONDITION_BY_ALL;

                }

            }
        });


        return new AlertDialog.Builder(getActivity()).setView(v).setTitle("Search").setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int witch) {
                        // TODO: 2018/12/22 这里使用原来的fragment 显示搜索结果
                        sendResult(Activity.RESULT_OK, mSearchDate, mConditionCode);

                    }
                })
                .create();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_DATE) {
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            mSearchDate = date;
        }
    }

    private void sendResult(int resultCode, Date date, int conditionCode) {
        if (getTargetFragment() == null) {
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(EXTRA_SEARCH_DATE, date);
        intent.putExtra(EXTRA_SEARCH_CONDITION, conditionCode);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }
}
