package com.jt.moneybook.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.jt.moneybook.models.Cost;
import com.jt.moneybook.database.CostDbSchema.CostTable;

import java.util.Date;
import java.util.UUID;

public class CostCursorWrapper extends CursorWrapper {
    public CostCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Cost getCost() {
        String uuidString = getString(getColumnIndex(CostTable.Cols.UUID));
        String title = getString(getColumnIndex(CostTable.Cols.TITLE));
        String money = getString(getColumnIndex(CostTable.Cols.MONEY));
        long date = getLong(getColumnIndex(CostTable.Cols.DATE));
        String method = getString(getColumnIndex(CostTable.Cols.METHOD));
        String direction = getString(getColumnIndex(CostTable.Cols.DIRECTION));
        String more = getString(getColumnIndex(CostTable.Cols.MORE));

        Cost cost = new Cost(UUID.fromString(uuidString));
        cost.setTitle(title);
        cost.setDate(new Date(date));
        cost.setMoney(money);
        cost.setMethod(method);
        cost.setDirection(direction);
        cost.setMore(more);

        return cost;
    }
}
