package com.jt.moneybook.database;

public class CostDbSchema {
    public static final class CostTable {
        public static final String NAME = "costs";

        public static final class Cols {
            //uuid title date money
            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DATE = "date";
            public static final String MONEY = "money";
            public static final String METHOD = "method";
            public static final String DIRECTION = "direction";
            public static final String MORE = "more";
        }
    }

    public static final class UserTable {
        public static final String NAME = "users";

        public static final class Cols {
            public static final String ACCOUNT = "account";
            public static final String PASSWORD = "password";

        }
    }
}
