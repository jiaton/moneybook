package com.jt.moneybook.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jt.moneybook.database.CostDbSchema.CostTable;
import com.jt.moneybook.database.CostDbSchema.UserTable;

/*此类用来帮助打开数据库*/
public class CostBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "costBase.db";

    public CostBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /*如果首次创建数据库，则会调用此方法*/
        sqLiteDatabase.execSQL("create table " + CostTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                CostTable.Cols.UUID + "," +
                CostTable.Cols.TITLE + "," +
                CostTable.Cols.DATE + " INTEGER," +
                CostTable.Cols.METHOD + "," +
                CostTable.Cols.DIRECTION + "," +
                CostTable.Cols.MORE + "," +
                CostTable.Cols.MONEY + ")");

        sqLiteDatabase.execSQL("create table " + UserTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                UserTable.Cols.ACCOUNT + "," +
                UserTable.Cols.PASSWORD + ")");

        ContentValues values = new ContentValues();
        values.put(UserTable.Cols.ACCOUNT, "admin");
        values.put(UserTable.Cols.PASSWORD, "admin");
        sqLiteDatabase.insert(UserTable.NAME, null, values);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        /*若helper中版本比本地数据库版本更高，则调用此方法升级*/
    }
}
